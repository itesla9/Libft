/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yteslenk <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 15:23:17 by yteslenk          #+#    #+#             */
/*   Updated: 2016/12/06 13:55:47 by yteslenk         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strequ(char const *s1, char const *s2)
{
	int	i;

	i = 0;
	if (!s1 || !s2)
		return (0);
	while (s1[i])
	{
		if (*((unsigned char *)s1 + i) != *((unsigned char *)s2 + i))
			return (0);
		i++;
		if (*((unsigned char *)s1 + i) != *((unsigned char *)s2 + i))
			return (0);
	}
	return (1);
}
